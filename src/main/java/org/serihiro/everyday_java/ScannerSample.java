package org.serihiro.everyday_java;

import java.util.Scanner;

// https://www.hackerrank.com/challenges/java-stdin-and-stdout-1
public class ScannerSample
{
    public static void main(String args[])
    {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();

        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
    }
}
