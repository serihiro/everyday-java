package org.serihiro.everyday_java;

import java.util.Scanner;

// https://www.hackerrank.com/challenges/java-stdin-stdout
public class ScannerSample2
{
    public static void main(String args[])
    {
        Scanner scan = new Scanner(System.in).useDelimiter("\r\n");
        int i = scan.nextInt();
        double d = scan.nextDouble();
        String s = scan.next();
        // Scanner use space as default delimiter.
        // So if you do not specify delimiter with `useDelimiter`,
        // you need to do this
        //
        // String s = "";
        // while(scan.hasNext()){
        //     if(s != ""){
        //         s += " " + scan.next();
        //     }else{
        //         s += scan.next();
        //     }
        // }
        System.out.println("String: " + s);
        System.out.println("Double: " + d);
        System.out.println("Int: " + i);
    }
}
