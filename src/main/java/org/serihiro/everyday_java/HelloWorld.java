package org.serihiro.everyday_java;

// https://www.hackerrank.com/challenges/welcome-to-java
public class HelloWorld
{
    public static void main(String[] args)
    {
        System.out.println("Hello, world!");
    }
}
