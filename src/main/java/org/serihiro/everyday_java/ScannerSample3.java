package org.serihiro.everyday_java;

import java.util.Scanner;

// https://www.hackerrank.com/challenges/java-output-formatting
public class ScannerSample3
{
    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("================================");
        for (int i = 0; i < 3; i++) {
            String s1 = sc.next();
            int x = sc.nextInt();
            System.out.print(s1);
            for (int adjustPointer = s1.length(); adjustPointer < 15; adjustPointer++) {
                System.out.print(" ");
            }
            System.out.println(String.format("%03d", x));
        }
        System.out.println("================================");
    }
}
