package org.serihiro.everyday_java;

import java.util.Scanner;

// https://www.hackerrank.com/challenges/java-if-else
public class IfElseSample
{
    public static void main(String args[])
    {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        if (n % 2 != 0) {
            System.out.println("Weird");
            return;
        }

        String answer = "Not Weird";
        if (2 <= n && n <= 5) {
            answer = "Not Weird";
        }
        else if (6 <= n && n <= 20) {
            answer = "Weird";
        }
        System.out.println(answer);
    }
}
